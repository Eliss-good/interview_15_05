### Для запуска проетка потребуется установить poetry

#### Шаги по запуску
1. [x] Клонируем репозиторий
2. [x] Заходим в папаку проекта, a далее в папку src
3. [x] Открывем консоль и пишем poetry install --only main
4. [x] Запускаем первое задание [poetry run task1](````)
5. [x] Запускаем второе задание [poetry run task2](````)
6. [x] Запускаем третье задание [poetry run task3](````)


### Архитектура папок:
 * [results](./src/results) - результат выполнения для третьего задания
 * [main.py](./src/main.py) - запуск всех задач
 * [.env](./src/.env) - настройка запуска
 * [tasks](./src/tasks) - список заданий
    * [taskOne](./src/tasks/taskOne.py) 
    * [taskTwo](./src/tasks/taskTwo.py) 
    * [taskThree](./src/tasks/taskThree.py)
 * [DataSource](./src/DataSource.py) - подключение к файлу excel


##### Результат выполнения для первого задания
```
[
     {
          "dt":"2022-12-01T00:00:00.000",
          "typeFluid":"oil_split",
          "wellId":92,
          "valueFluid":58.393
     },
     {
          "dt":"2022-12-04T00:00:00.000",
          "typeFluid":"oil_split",
          "wellId":188,
          "valueFluid":65.99
     },
     {
          "dt":"2022-12-05T00:00:00.000",
          "typeFluid":"oil_split",
          "wellId":253,
          "valueFluid":92.679
     },
     {
          "dt":"2022-12-05T00:00:00.000",
          "typeFluid":"oil_split",
          "wellId":283,
          "valueFluid":85.405
     },
     {
          "dt":"2022-12-01T00:00:00.000",
          "typeFluid":"gas_split",
          "wellId":154,
          "valueFluid":84.854
     },
     {
          "dt":"2022-12-10T00:00:00.000",
          "typeFluid":"gas_split",
          "wellId":4,
          "valueFluid":58.981
     },
     {
          "dt":"2022-12-05T00:00:00.000",
          "typeFluid":"water_split",
          "wellId":289,
          "valueFluid":66.344
     },
     {
          "dt":"2022-12-06T00:00:00.000",
          "typeFluid":"water_split",
          "wellId":90,
          "valueFluid":4.419
     }
]
```
