import pandas as pd
from functools import wraps


def checkFieldExist(listField=None):
    """ Проверяет есть ли все нужные столбцы в DataFrame  
        listField: список названий столбцов
    """
    def wrapper(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            df = func(*args, **kwargs)

            if listField:
                columnNotLoad = list(set(listField) - set(df.columns.values.tolist()))
                if len(columnNotLoad) > 0:
                    raise Exception(f"Отсутсвуют cтолбцы {columnNotLoad}")
                
            return df

        return wrapped

    return wrapper


class DataSource:
    __slots__ = ["nameFile"]

    def __init__(self, nameFile: str):
        if not nameFile:
            raise FileExistsError("not set file name")

        self.nameFile = nameFile

    @checkFieldExist(["dt", "well_id", "oil_rate", "gas_rate", "water_rate"])
    def readRates(self):
        return pd.read_excel(self.nameFile, "rates")

    @checkFieldExist(
        ["dt", "well_id", "layer_id", "oil_split", "gas_split", "water_split"]
    )
    def readSplit(self):
        return pd.read_excel(self.nameFile, "splits")

    @checkFieldExist(
        ["dt", "well_id", "layer_id", "oil_split", "gas_split", "water_split"]
    )
    def readInvalidSplit(self):
        return pd.read_excel(self.nameFile, "invalid_splits")
