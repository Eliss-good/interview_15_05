import pandas as pd


def taskTwo(dfSplits: pd.DataFrame, dfRate: pd.DataFrame) -> pd.DataFrame:
    mergeSplitRate = pd.merge(dfSplits, dfRate,  how='left', left_on=['well_id','dt'], right_on = ['well_id','dt'])
    #объеденяю таблицу dfRate к dfSplits
    
    prefixFliud = ['oil', 'gas', 'water'] # ввожу префикс флюидов
    
    for onePrefix in prefixFliud:
        #rate = split * rate / 100
        mergeSplitRate[f"{onePrefix}_rate"] = mergeSplitRate[f"{onePrefix}_rate"] * mergeSplitRate[f"{onePrefix}_split"] / 100

    return mergeSplitRate
