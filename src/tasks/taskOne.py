import pandas as pd
from typing import TypedDict, List
from datetime import datetime
from functools import reduce


class ResultTaskOne(TypedDict):
    dt: datetime
    typeFluid: str
    wellId: int
    valueFluid: float


def handlerReducAcc(
    accResult: pd.DataFrame, newDataRows: pd.DataFrame, oneNameFluid: str
):
    """
    accResult: аккумулируемый результат
    newDataRows: отфильтрованные строки для присоеиденения к accResult
    oneNameFluid: название столбца флюида
    """
    newDataRows["typeFluid"] = oneNameFluid
    newDataRows["valueFluid"] = newDataRows[oneNameFluid]
    newDataRows = newDataRows.rename(columns={"well_id": "wellId"})
    #все строки выше нужны для форматирования датафрейма в нужный формат ответа - ResultTaskOne

    return pd.concat(
        [
            accResult,
            newDataRows[
                list(ResultTaskOne.__annotations__.keys())
            ],
        ],
        ignore_index=True
    )


def taskOne(dfInvalidSplits: pd.DataFrame, roundValue: int = 3):
    allFluidName = ["oil_split", "gas_split", "water_split"]# Список столбцов флюидов
    dfInvalidSplits["dt"] = pd.to_datetime(dfInvalidSplits["dt"]).dt.date #Оставляю только дату  

    dtSplitsGroups = dfInvalidSplits.groupby(by=["dt", "well_id"])[allFluidName].sum() #Считаю суммы флюидов в рамках даты и скаважину

    dtSplitsGroups = dtSplitsGroups[allFluidName].round(roundValue) #Округляем для избежания ошибки с окрунлением
    accReturnRows = pd.DataFrame(columns=ResultTaskOne.__annotations__) #Сюда складируется список всех подходящих флюидов в рамках задачи

    for oneNameFluid in allFluidName:
        accReturnRows = handlerReducAcc(
            accResult=accReturnRows,
            newDataRows=dtSplitsGroups.loc[
                dtSplitsGroups[oneNameFluid] < 100 #Беру строки меньше 100 в рамках одного флюида
            ].reset_index(),
            oneNameFluid=oneNameFluid,
        )

    print(accReturnRows.to_json(date_format="iso", orient="records", indent=5)) 
