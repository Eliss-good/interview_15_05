from tasks.taskTwo import taskTwo
import pandas as pd
from json import loads, dumps



def saveExcel(df: pd.DataFrame, nameExcelFileSave: str):
    df["dt"] = df["dt"].dt.strftime("%Y-%m-%d %H:%M:%S") 
    #форматируем стольец даты, так как в excel стобец выводится с ошибкой без форматирования в нужный формат
    df.to_excel(nameExcelFileSave)


def saveJson(df: pd.DataFrame, nameJsonFileSave: str): 

    jsonToWrite = df.rename(
        columns={
            "well_id": "wellId",
            "layer_id": "layerId",
            "oil_rate": "oilRate",
            "gas_rate": "gasRate",
            "water_rate": "waterRate",
        }
    ).to_json(orient="records", date_format="iso") # перевожду столбцы под нужное API
    

    with open(nameJsonFileSave, 'w') as f:
        f.write(dumps({"allocation": {"data": loads(jsonToWrite)}}, indent=4))


def taskThree(
    dfSplits: pd.DataFrame,
    dfRate: pd.DataFrame,
    nameExcelFileSave: str,
    nameJsonFileSave: str,
):
    resultTaskTwo = taskTwo(dfSplits=dfSplits, dfRate=dfRate) # получаем результат из второго задания
    
    resultTaskTwoFilterColumn: pd.DataFrame = resultTaskTwo[
        ["dt", "well_id", "layer_id", "oil_rate", "gas_rate", "water_rate"]
    ]# оставляем нужные столбцы
    saveExcel(resultTaskTwoFilterColumn.copy(), nameExcelFileSave)
    saveJson(resultTaskTwoFilterColumn.copy(), nameJsonFileSave)