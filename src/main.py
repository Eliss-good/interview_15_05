import os

from dotenv import load_dotenv
from DataSource import DataSource

from tasks.taskOne import taskOne
from tasks.taskTwo import taskTwo
from tasks.taskThree import taskThree

load_dotenv()#подгрузка всех переменных окружения с .env

dataSource = DataSource(os.getenv("NAME_EXCEL_FILE"))# Подключение к файлу excel

def startTaskOne():
    # вызов задания 1
    taskOne(dataSource.readInvalidSplit())

def startTaskTwo():
    # вызов задания 2
    taskTwo(dataSource.readSplit(), dataSource.readRates())

def startTaskThree():
    # вызов задания 3

    taskThree(
        dataSource.readSplit(),
        dataSource.readRates(),
        os.getenv("NAME_EXCEL_FILE_SAVE_TASK_TWO"),
        os.getenv("NAME_JSON_FILE_SAVE_TASK_TWO"),
    )
